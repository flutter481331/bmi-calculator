import 'package:flutter/material.dart';
import 'screens/input_page.dart';

void main() => runApp(BMICalculator());

class BMICalculator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme(
            primary: Colors.white,
            surface: Color(0xFF0A0E21),
            onSurface: Colors.white,
            onPrimary: Colors.white,
            brightness: Brightness.dark,
            secondary: Colors.greenAccent,
            onSecondary: Color(0xFF0A0E21),
            error: Colors.red,
            onError: Colors.white,
            background: Colors.orange,
            onBackground: Colors.orange,
            secondaryContainer: Color(0xFF0A0E21),
            primaryContainer: Colors.white,
            onPrimaryContainer: Colors.white),
        scaffoldBackgroundColor: Color(0xFF0A0E21),
      ),
      // .copyWith(scaffoldBackgroundColor: colorScheme.background),
      home: InputPage(),
    );
  }
}
