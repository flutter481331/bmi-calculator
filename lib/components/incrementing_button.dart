import 'package:bmi_calculator/components/round_icon_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../constants.dart';

class IncrementingButton extends StatefulWidget {
  IncrementingButton(
      {required this.label,
      required this.incrementingText,
      required this.minValue,
      required this.maxValue});

  final String label;
  int incrementingText;
  final int maxValue;
  final int minValue;

  @override
  State<IncrementingButton> createState() => _IncrementingButtonState(
      label: label,
      incrementingLabel: incrementingText,
      minValue: minValue,
      maxValue: maxValue);
}

class _IncrementingButtonState extends State<IncrementingButton> {
  _IncrementingButtonState(
      {required this.label,
      required this.incrementingLabel,
      required this.maxValue,
      required this.minValue});

  final String label;
  int incrementingLabel;
  final int maxValue;
  final int minValue;

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(this.label, style: kLabelTextStyle),
          Text(incrementingLabel.toString(), style: kNumberTextStyle),
          SizedBox(width: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RoundIconButton(
                onPress: () {
                  setState(() {
                    incrementingLabel >= 1
                        ? incrementingLabel = incrementingLabel - 1
                        : incrementingLabel = 0;
                  });
                },
                icon: FontAwesomeIcons.minus,
              ),
              SizedBox(width: 5.0),
              RoundIconButton(
                onPress: () {
                  setState(() {
                    incrementingLabel < 300
                        ? incrementingLabel = incrementingLabel + 1
                        : incrementingLabel = 300;
                  });
                },
                icon: FontAwesomeIcons.plus,
              ),
            ],
          )
        ]);
  }
}
